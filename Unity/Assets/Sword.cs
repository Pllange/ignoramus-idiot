﻿using UnityEngine;
using System.Collections;

public class Sword : MonoBehaviour {
    private AudioSource pickUp;
    public AudioClip hit1;
    public AudioClip hit2;
    public AudioClip hit3;
	// Use this for initialization
    void Start()
    {

        pickUp = gameObject.GetComponent<AudioSource>();
        switch(Random.Range(1,3))
        {
            case 1:
                    Debug.Log("1");
                    pickUp.clip = hit1;
                    break;
            case 2:
                    Debug.Log("2");
                    pickUp.clip = hit2;
                    break;
            case 3:
                    Debug.Log("3");
                    pickUp.clip = hit3;
                    break;
                
            default:
                
                    break;
                
        }

        pickUp.Play();

    }

    // Update is called once per frame
    void Update()
    {

        if (!pickUp.isPlaying)
            Destroy(gameObject);


    }
}
