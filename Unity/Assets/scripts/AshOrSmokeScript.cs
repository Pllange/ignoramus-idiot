﻿using UnityEngine;
using System.Collections;

public class AshOrSmokeScript : MonoBehaviour {
    public GameObject coin;
    
	// Use this for initialization
	void Start () {
        Destroy(gameObject, 0.3f);
	}
	
	// Update is called once per frame
	void OnDestroy() {
        Instantiate(coin, transform.position, new Quaternion(0, 0, 0, 0));
        
	}
}
