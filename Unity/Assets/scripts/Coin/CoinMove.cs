﻿using UnityEngine;
using System.Collections;

public class CoinMove : MonoBehaviour {
    public Vector2 target;
    private float step;
    public float speed;
    public int gaining;
    public GameObject[] gainingPlayer = new GameObject[2];
	// Use this for initialization
	void Start () {
        gainingPlayer[0] = GameObject.FindGameObjectWithTag("player1");
        gainingPlayer[1] = GameObject.FindGameObjectWithTag("player2");
	}
	
	// Update is called once per frame
	void Update () {
        step = speed * Time.deltaTime;
        transform.position = Vector2.MoveTowards(transform.position, target, step);
        if (transform.position.x == target.x && transform.position.y == target.y)
        {
            gainingPlayer[gaining].GetComponent<Movement>().coin += 1;
            Destroy(gameObject);
        }
	}
}
