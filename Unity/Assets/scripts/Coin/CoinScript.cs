﻿using UnityEngine;
using System.Collections;

public class CoinScript : MonoBehaviour {
    public int collededWith;
    public int gaining;
    public GameObject[] gainingPlayer = new GameObject[2];
    public GameObject coinPickUp;
    public GameObject coinP1;
    public GameObject coinP2;
    public Vector2 destination;
    public float distance;
    public GameManeger gamemaneger;
    public Vector2 startPos;
    public float speed;
    public float IMFLYING;
    private float minXPoS;
    private float minYPos;
    private float maxXPos;
    private float maxYPos;
    private float despawnTime;
	// Use this for initialization
	void Start () {
        gamemaneger = GameObject.FindGameObjectWithTag("GameManeger").GetComponent<GameManeger>();
        despawnTime = gamemaneger.despawnTime;
        minXPoS = gamemaneger.minXPoS;
        minYPos = gamemaneger.minYPos;
        maxXPos = gamemaneger.maxXPos;
        maxYPos = gamemaneger.maxYPos;
        speed = gamemaneger.coinSpeed;
        gainingPlayer[0] = GameObject.FindGameObjectWithTag("player1");
        gainingPlayer[1] = GameObject.FindGameObjectWithTag("player2");
        destination = new Vector2(Random.Range(transform.position.x -2f,transform.position.x +2f),Random.Range(transform.position.y-2,transform.position.y +2));
        distance = Random.Range(1f,3.5f);
        startPos=transform.position;
        Destroy(gameObject, despawnTime);
        
	}
	
	// Update is called once per frame
	void Update () {
        if (gamemaneger.flyingCoinsInDev&&IMFLYING<1)
        {
            IMFLYING += (Time.deltaTime*2);
            transform.position = Vector2.MoveTowards(transform.position, destination,distance*Time.deltaTime);
        }
        else if (gamemaneger.flyingCoinsInDev && IMFLYING > 1)
        {
            
        }

	}
    void OnTriggerStay(Collider other)
    {
        if (IMFLYING>1 || !gamemaneger.flyingCoinsInDev)
        {

            if (Vector2.Distance(transform.position, gainingPlayer[0].transform.position) < Vector2.Distance(transform.position, gainingPlayer[1].transform.position))
            {
                gaining = 0;
                if (other.transform.tag == "player1")
                {
                GameObject go = Instantiate(coinP1, transform.position, coinP1.transform.rotation) as GameObject;
                go.GetComponent<CoinMove>().target = new Vector2(-7.25f, -3.75f);
                go.GetComponent<CoinMove>().gaining = gaining;
                Instantiate(coinPickUp);
                Destroy(gameObject);
                }
            }
            else
            {
                gaining = 1;
                if (other.transform.tag == "player2")
                {
                    GameObject go = Instantiate(coinP2, transform.position, coinP1.transform.rotation) as GameObject;
                    go.GetComponent<CoinMove>().target = new Vector2(6.25f, -3.75f);
                    go.GetComponent<CoinMove>().gaining = gaining;
                    Instantiate(coinPickUp);
                    Destroy(gameObject);
                }
            }
        }
    }
}
