﻿using UnityEngine;
using System.Collections;

public class GUIMan : MonoBehaviour
{
    #region verbable
    public Font CAESAR;
    private float plr1startx;
    private float plrstarty;
    private float plr2startx;
    private float playerwidth;
    private float playerheight;
    private float staminaP1;
    private float staminaP2;
    private float P1A;
    private float P2A;
    private float P2B;
    private float P1B;
    public float timesec;
    private int killP1;
    private int killP2;
    private int coinP1;
    private int coinP2;
    private int mostcoin;
    private int winnerCoin;
    private int winnerkill;
    public int timemin;
    private bool story;
    private bool ops;
    private bool ops1;
    public bool inGame;
    public bool endGame;
    private string winner;
    private GUIStyle Green;
    private GUIStyle Red;
    public GUIStyle Text;
    public GameObject player1gm;
    public GameObject player2gm;
    public GameObject trup;
    private GameManeger gameManeger;
    private Vector2 player1pos;
    private Vector2 player2pos;
    public Movement movement1;
    private Movement movement2;
    public Texture coin;
    public Texture kill;
    public Texture player;
    public Texture player2;
    public Texture turtorial;
    public Texture P1Back;
    public Texture P2Back;
    public Texture storytxt;
    public Texture crown;
    public Texture winnerP1;
    public Texture winnerP2;
    public Texture Tied;
    #endregion

    // Use this for initialization
	void Start () {
        Text.font = CAESAR;
        Text.fontSize = 40;
        plr1startx = 0;
        plr2startx = Screen.width * 23 / 32;
        plrstarty = Screen.height * 3 / 4;
        playerwidth = Screen.width * 9 / 32;
        playerheight = Screen.height * 9 / 32;
        timemin = 1;
        timesec = 30;
        player1gm = GameObject.FindGameObjectWithTag("player1");
        player2gm = GameObject.FindGameObjectWithTag("player2");
        movement1 = player1gm.GetComponent<Movement>();
        movement2 = player2gm.GetComponent<Movement>();
        gameManeger = GameObject.FindGameObjectWithTag("GameManeger").GetComponent<GameManeger>();
        //inGame = true;
        
        
	}

    void FillNull()
    {
        
    }
	
	// Update is called once per frame
	void Update () 
    {
        if (coinP1 > coinP2)
        {
            mostcoin = 1;
        }
        else if (coinP1 == coinP2)
        {
         
        }
        else
        {
            mostcoin = 2;
        }
        if (Input.GetButtonDown("EndGame") && endGame)
        {
            Application.LoadLevel(Application.loadedLevelName);
        }
        if (Input.GetButtonDown("Test") && !inGame && story || Input.GetKeyDown(KeyCode.Escape) && !inGame && story)
        {
            inGame = true;
            if(!endGame)
            GameObject.Find("spawnManeger").GetComponent<SpawnManeger>().spawn += 1.5f;
        }
        if (Input.GetButtonDown("Test") && !inGame && !story || Input.GetKeyDown(KeyCode.Escape) && !inGame && !story)
        {
            story = true;
        }
        #region timer
        //timer
        if (timesec <= 0 && timemin>0)
        {
            timemin--;
            timesec += 60;
        }
        if (timesec > 0 && inGame)
            timesec -= Time.deltaTime;
        #endregion
        #region setting game

        killP1 = movement1.killz;
        coinP1 = movement1.coin;
        staminaP1 = movement1.stamina;
        killP2 = movement2.killz;
        coinP2 = movement2.coin;
        staminaP2 = movement2.stamina;
        player1pos = new Vector2(player1gm.transform.position.x, player1gm.transform.position.y);
        player2pos = new Vector2(player2gm.transform.position.x, player2gm.transform.position.y);
        #endregion
        if (timemin <= 0 && timesec <= 0 && !endGame || Input.GetKey(KeyCode.A)&&Input.GetKey(KeyCode.I))
        {
            endGame = true;
            inGame = false;
            Instantiate(trup);
        }
        #region Opasity
        player2pos = new Vector2(player2gm.transform.position.x, player2gm.transform.position.y);
        player1pos = new Vector2(player1gm.transform.position.x, player1gm.transform.position.y);
        P1A = (Vector2.Distance(player1pos, new Vector2(-6, -3)) / 4) + 0.3f;
        P2A = (Vector2.Distance(player2pos, new Vector2(-6, -3)) / 4) + 0.3f;
        P1B = (Vector2.Distance(player1pos, new Vector2(6, -3)) / 4) + 0.3f;
        P2B = (Vector2.Distance(player2pos, new Vector2(6, -3)) / 4) + 0.3f;
        if (P1A < P2A)
        {
            ops = true;
        }
        else
        {
            ops = false;
        }
        if (P1B < P2B)
        {
            ops1 = true;
        }
        else
        {
            ops1 = false;
        }
        
        #endregion

    }
    void OnGUI()
    {
        
        #region endgame

        if (endGame)
        {
            if (coinP1 > coinP2)
            {
                GUI.Box(new Rect(0, 0, Screen.width, Screen.height), winnerP1, GUIStyle.none);
                winner = "PLAYER 1 WINS";
                winnerCoin = coinP1;
                winnerkill = killP1;
            }
            else if (coinP2 > coinP1)
            {
                GUI.Box(new Rect(0, 0, Screen.width, Screen.height), winnerP2, GUIStyle.none);
                winner = "PLAYER 2 WINS";
                winnerCoin = coinP2;
                winnerkill = killP2;
            }
            else
            {
                GUI.Box(new Rect(0, 0, Screen.width, Screen.height), Tied, GUIStyle.none);
                winner = "TIED YOU BOTH LOSE";
            }
            
            
            

        }
        #endregion
        #region menu
        if (!inGame && !endGame&& story)
        {
            GUI.backgroundColor = new Color(1, 1, 1, 0);
            GUI.Box(new Rect(0, Screen.height*1/8, Screen.width, Screen.height), turtorial);
            GUI.backgroundColor = Color.white;
            //GUI.Box(new Rect(Screen.width * 23 / 64, Screen.height * 20 / 32, 0, 0), " <size=40>"+" press start"+" </size>", GUIStyle.none);

        }
        if (!inGame && !endGame&& !story)
        {
            GUI.Box(new Rect(0, 0, Screen.width, Screen.height), storytxt,GUIStyle.none);
            //GUI.Box(new Rect(Screen.width * 23 / 64, Screen.height * 20 / 32, 0, 0), " <size=40>"+" press start"+" </size>", GUIStyle.none);

        }
        #endregion
        InitStyles();
        if (inGame)
        {
            #region timer
            GUI.Box(new Rect(Screen.width * 3 / 8, 0, Screen.width * 2 / 8, Screen.height * 1 / 8), "<size=60>" + timemin.ToString() + " : " + Mathf.Round(timesec).ToString() + "</size>");
            #endregion
            #region player 1 gui
            //player 1
            //GUI.Box(new Rect(plr1startx, plrstarty, playerwidth, playerheight),"");
            GUI.BeginGroup(new Rect(plr1startx, plrstarty, playerwidth , playerheight ));
            #region oldgui
            //stamina bar
            Color oldCol = GUI.color;
            if(ops==true)
            GUI.color = new Color(oldCol.r, oldCol.g, oldCol.b, P1A);
            else
            GUI.color = new Color(oldCol.r, oldCol.g, oldCol.b, P2A);
            GUI.Box(new Rect(0 ,0 ,playerwidth , playerheight), P1Back, GUIStyle.none);
            //player image
            
            
            GUI.Box(new Rect(playerwidth * 6 / 64, plrstarty * 4 / 32, playerwidth * 3 / 16, playerheight * 3 / 8), player, GUIStyle.none);
            if (mostcoin == 1)
            GUI.Box(new Rect(playerwidth * 10 / 128, plrstarty * 2 / 32, playerwidth * 3 / 8, playerheight * 3 / 16), crown, GUIStyle.none);
            //kill count
            GUI.Box(new Rect(playerwidth * 37 / 48, playerheight * 8 / 16, playerwidth * 3 / 16, playerheight * 6 / 16), kill, GUIStyle.none);
            //kill num
            GUI.Box(new Rect(playerwidth * 20 / 24, playerheight * 2 / 16, playerwidth * 2 / 8, playerheight * 2 / 8), "<size=40>" + killP1.ToString() +"</size>", Text);
            //coins
            GUI.Box(new Rect(playerwidth * 21 / 48, playerheight * 8 / 16, playerwidth * 6 / 32, playerheight * 5 / 16), coin,GUIStyle.none);
            //coin num
            GUI.Box(new Rect(playerwidth * 12 / 24, playerheight * 2 / 16, playerwidth * 2 / 8, playerheight * 2 / 8), "<size=40>" + coinP1.ToString() + "</size>", Text);
            //around bar
            GUI.BeginGroup(new Rect((playerwidth * 1 / 16), (playerheight * 17 / 24), playerwidth * 16 / 16, playerheight * 1 / 4));
            //GUI.Box(new Rect(0,0, Screen.height,Screen.width), "test");
            //GUI.Box(new Rect(-10, (playerheight * 1 / 24)-10, playerwidth * 6 / 8, playerheight * 2 / 4), "yolo");
            //bar
            GUI.BeginGroup(new Rect((playerwidth * 1 / 256), playerheight * 1 / 24, playerwidth * 54 / 64, playerheight * 1 / 4));
            //GUI.Box(new Rect(0, 0, Screen.height, Screen.width), "test");
            GUI.color = oldCol;
            #endregion
            #region health
            /*if (gameManeger.playerHealth)
            {
                GUI.BeginGroup(new Rect((playerwidth * 1 / 256), playerheight * 1 / 24, playerwidth * 54 / 64, playerheight * 1 / 4));
                //bar
                //GUI.Box(new Rect(0, 0, Screen.height, Screen.width), "test");
                GUI.Box(new Rect(-5, -5, (playerwidth * 54 / 64) + 10, (playerheight * 1 / 4) + 10), "", Red);
                GUI.Box(new Rect(-5, -5, ((playerwidth * 54 / 64) + 10) * (staminaP1 / 100), (playerheight * 1 / 4) + 10), "", Green);
                GUI.EndGroup();
            }*/
            #endregion
            GUI.EndGroup();
            GUI.EndGroup();
            GUI.EndGroup();
            #endregion
            #region player 2 gui
            //player 1
            GUI.BeginGroup(new Rect(plr2startx, plrstarty, playerwidth, playerheight));
            //stamina bar
            Color OoldCol = GUI.color;
            if(ops1==true)
            GUI.color = new Color(oldCol.r, oldCol.g, oldCol.b, P1B);
            else
            GUI.color = new Color(oldCol.r, oldCol.g, oldCol.b, P2B);
            GUI.Box(new Rect(0, 0, playerwidth, playerheight), P2Back,GUIStyle.none);
            
            //player image
            GUI.Box(new Rect(playerwidth * 6 / 64, plrstarty * 4 / 32, playerwidth * 3 / 16, playerheight * 3 / 8), player2, GUIStyle.none);
            if (mostcoin == 2)
                GUI.Box(new Rect(playerwidth * 10 / 128, plrstarty * 2 / 32, playerwidth * 3 / 8, playerheight * 3 / 16), crown, GUIStyle.none);
            //kill count
            GUI.Box(new Rect(playerwidth * 37 / 48, playerheight * 8 / 16, playerwidth * 3 / 16, playerheight * 6 / 16), kill, GUIStyle.none);
            //kill num
            GUI.Box(new Rect(playerwidth * 20 / 24, playerheight * 2 / 16, playerwidth * 2 / 8, playerheight * 2 / 8), "<size=40>" + killP2.ToString() + "</size>", Text);
            //coins
            GUI.Box(new Rect(playerwidth * 21 / 48, playerheight * 8 / 16, playerwidth * 6 / 32, playerheight * 5 / 16), coin, GUIStyle.none);
            //coin num
            GUI.Box(new Rect(playerwidth * 12 / 24, playerheight * 2 / 16, playerwidth * 2 / 8, playerheight * 2 / 8), "<size=40>" + coinP2.ToString() + "</size>", Text);
            //around bar
            GUI.BeginGroup(new Rect((playerwidth * 1 / 256), playerheight * 1 / 24, playerwidth * 54 / 64, playerheight * 1 / 4));
            //GUI.Box(new Rect(0, 0, Screen.height, Screen.width), "test");
            GUI.color = oldCol;
            #region health
            /*if (gameManeger.playerHealth)
            {
                GUI.BeginGroup(new Rect((playerwidth * 1 / 256), playerheight * 1 / 24, playerwidth * 54 / 64, playerheight * 1 / 4));
                //bar
                //GUI.Box(new Rect(0, 0, Screen.height, Screen.width), "test");
                GUI.Box(new Rect(-5, -5, (playerwidth * 54 / 64) + 10, (playerheight * 1 / 4) + 10), "", Red);
                GUI.Box(new Rect(-5, -5, ((playerwidth * 54 / 64) + 10) * (staminaP2 / 100), (playerheight * 1 / 4) + 10), "", Green);
                GUI.EndGroup();
            }*/
            #endregion
            GUI.EndGroup();
            GUI.EndGroup();
            //GUI.EndGroup();
            #endregion
        }
        

    }
    private void InitStyles()
    {
        //color
        if (Green == null)
        {
            Green = new GUIStyle(GUI.skin.box);
            Green.normal.background = MakeTex(2, 2, new Color(1, 0.92f, 0.016f, 1));
        }
        if (Red == null)
        {
            Red = new GUIStyle(GUI.skin.box);
            Red.normal.background = MakeTex(2, 2, new Color(1, 0, 0, 0.5f));
        }
        
        /*
        if (Overload == null)
        {
            Overload = new GUIStyle(GUI.skin.box);
            Overload.normal.background = MakeTex(2, 2, new Color(1, 0.92f, 0.016f, 0.5f));
        }
        if (Overload2 == null)
        {
            Overload2 = new GUIStyle(GUI.skin.box);
            Overload2.normal.background = MakeTex(2, 2, new Color(1, 0, 0, 0.75f));
        }
         */

    }

    private Texture2D MakeTex(int width, int height, Color col)
    {
        Color[] pix = new Color[width * height];
        for (int i = 0; i < pix.Length; ++i)
        {
            pix[i] = col;
        }
        Texture2D result = new Texture2D(width, height);
        result.SetPixels(pix);
        result.Apply();
        return result;
    }
}
