﻿using UnityEngine;
using System.Collections;

public class GameManeger : MonoBehaviour {
    public bool player;
    public float stamina;
    public float attackRange;
    public float pushRange;
    public float pushLength;
    public float pushedEndTime;
    public float smallPushedEndTime;
    public float dashLength;
    public int damege;
    public int baseSpeed;
    public int dashSpeed;
    public int minimumMovement;
    public float minXPoS;
    public float minYPos;
    public float maxXPos;
    public float maxYPos;
    public float attackWidth;
    public float attackCooldown;
    public float pushSpeed;
    public float dashPushSpeed;
    public float stunDuritation;

    public bool badguys;
    public float findTargetCooldown;
    public float badGuysAttackRange;
    public float attackMaxRange;
    public float attackingEndTime;
    public float attackValue;
    public int health;
    public float speed;
    public float distanceFromPlayer;
    public int coinsDropped;
    public float beingPushedSpeed;
    public float badDashPushSpeed;
    public float badPushedEndTime;
    public float badSmallPushedEndTime;

    public bool spawnManeger;
    public Vector3[] spawnPoint;
    public float spawnCooldown;
    public float moreSpawns;

    public bool Coins;
    public float coinSpeed;
    public float despawnTime;
    //public int killCoinsEveryXkill;

    public bool features;

    public bool playerHealth;

    public bool flyingCoinsInDev;

    public bool pushMonstersNotWorking;
    public bool bigMonsters;
    public int bigMonsterHealth;
    public int bigMonsterSpeed;
    public float bigMonsterScale;
    public int bigMonsterscoinsDropped;
    public float bigMonstersDistanceFromPlayer;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
