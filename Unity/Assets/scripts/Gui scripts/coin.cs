﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class coin : MonoBehaviour {
    private int coinP1;
    private int coinP2;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        coinP1 = GameObject.Find("Player 1").GetComponent<Movement>().coin;
        coinP2 = GameObject.Find("Player 2").GetComponent<Movement>().coin;
        
        if (gameObject.name == "P1")
        GetComponent<Text>().text = coinP1.ToString();
        if (gameObject.name == "P2")
        GetComponent<Text>().text = coinP2.ToString();
	}
}
