﻿using UnityEngine;
using System.Collections;

public class stamina : MonoBehaviour {
    private float staminaP1;
    private float staminaP2;
    private GUIStyle Green;
    private GUIStyle Red;
    
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        
	}
    void OnGUI()
    {
        InitStyles();
        GUI.Box(new Rect(-5, -5, (Screen.width * 1 / 4 * 54 / 64) + 10, (Screen.height * 1 / 4 * 1 / 4) + 10), "", Red);
        GUI.Box(new Rect(-5, -5, ((Screen.width * 1 / 4 * 54 / 64) + 10) * (staminaP1 / 100), (Screen.height * 1 / 4 * 1 / 4) + 10), "", Green);
    }
    private void InitStyles()
    {
        //color
        if (Green == null)
        {
            Green = new GUIStyle(GUI.skin.box);
            Green.normal.background = MakeTex(2, 2, new Color(1, 0.92f, 0.016f, 1));
        }
        if (Red == null)
        {
            Red = new GUIStyle(GUI.skin.box);
            Red.normal.background = MakeTex(2, 2, new Color(1, 0, 0, 0.5f));
        }

        /*
        if (Overload == null)
        {
            Overload = new GUIStyle(GUI.skin.box);
            Overload.normal.background = MakeTex(2, 2, new Color(1, 0.92f, 0.016f, 0.5f));
        }
        if (Overload2 == null)
        {
            Overload2 = new GUIStyle(GUI.skin.box);
            Overload2.normal.background = MakeTex(2, 2, new Color(1, 0, 0, 0.75f));
        }
         */

    }

    private Texture2D MakeTex(int width, int height, Color col)
    {
        Color[] pix = new Color[width * height];
        for (int i = 0; i < pix.Length; ++i)
        {
            pix[i] = col;
        }
        Texture2D result = new Texture2D(width, height);
        result.SetPixels(pix);
        result.Apply();
        return result;
    }
}
