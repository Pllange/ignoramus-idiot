﻿using UnityEngine;
using System.Collections;
using System;

public class Movement : MonoBehaviour 
{    
    public float MoveX;
    public float MoveY;
    private float stu;
    public float pushedTime;
    private float dashTime;
    public float stamina;
    public float attackRange;
    public float pushRange;
    public float pushLength;
    public float pushedEndTime;
    public float dashLength;
    private int speed;
    public int look = 1;
    public int playerID;
    public float pushed;
    public int ani;
    public int coin;
    public int damege;
    public int baseSpeed;
    public int dashSpeed;
    public int killz;
    public int minimumMovement;
  //  private bool canIMove;
    private bool CanMovePosX = true;
    private bool CanMoveNegX = true;
    private bool CanMovePosY = true;
    private bool CanMoveNegY = true;
    private bool attackButt;
    public bool moving;
    public bool regainStamina;
    private bool pushButt;
    private bool dashing;
    public string Joystick;
    private GameObject otherPlayer;
    private GameObject[] enemies;
    public GameObject Hit;
    public GameObject push;
    public float minXPoS;
    public float minYPos;
    public float maxXPos;
    public float maxYPos;
    public float attackWidth;
    public float attackCooldown;
    private float attackwait;
    public Animator anim;
    public anime Anime;
    public GameManeger gamemaneger;
    public GameObject pushedBy;
    public Vector2 pushedToPosition;
    public float pushSpeed;
    public float smallPushEndTime;
    private float pushStop;
    private int killCoinsGained;
    private int killCoinsEveryXkill;
    private GUIMan gUIman;
    public float stunDuritation;

    
    
	// Use this for initialization
	void Start () 
    {

        gUIman = Camera.main.GetComponent<GUIMan>();
        pushedTime = 1000;
        gamemaneger = GameObject.FindGameObjectWithTag("GameManeger").GetComponent<GameManeger>();
        
        stamina = gamemaneger.stamina;
        attackRange = gamemaneger.attackRange;
        pushRange = gamemaneger.pushRange;
        pushLength = gamemaneger.pushLength;
        pushedEndTime = gamemaneger.pushedEndTime;
        dashLength = gamemaneger.dashLength;
        damege = gamemaneger.damege;
        baseSpeed = gamemaneger.baseSpeed;
        dashSpeed = gamemaneger.dashSpeed;
        minimumMovement = gamemaneger.minimumMovement;
        minXPoS = gamemaneger.minXPoS;
        minYPos= gamemaneger.minYPos;
        maxXPos = gamemaneger.maxXPos;
        maxYPos = gamemaneger.maxYPos;
        attackWidth = gamemaneger.attackWidth;
        attackCooldown = gamemaneger.attackCooldown;
        pushSpeed = gamemaneger.pushSpeed;
        Anime = GetComponentInChildren<anime>();
        anim = gameObject.GetComponentInChildren<Animator>();
        smallPushEndTime = gamemaneger.smallPushedEndTime;
        stunDuritation = gamemaneger.stunDuritation;

        //canIMove = true;
        speed = baseSpeed;
        
        if (gameObject.tag == "player1")
        {
            otherPlayer = GameObject.FindGameObjectWithTag("player2");
        }
        else if (gameObject.tag == "player2")
        {
            otherPlayer = GameObject.FindGameObjectWithTag("player1");
        }
        else
        {
            //Debug.Log("contact your nearest peter");
        }
       // Debug.Log(FindAngle(transform.forward, otherPlayer.transform.position - transform.position, transform.up));
        //Debug.Log(otherPlayer.transform.position - transform.position);
        //Debug.Log(transform.forward);
        //Debug.Log(transform.up);
        //Debug.Log(Vector2.Angle(Vector2.up,otherPlayer.transform.position-transform.position));
      //  Debug.Log(playerID +" "+ FindAngle(transform.forward,otherPlayer.transform.position-transform.position, transform.forward) * (180 / Math.PI));
	}
	
	// Update is called once per frame
    void Update()
    {
        if (!gUIman.endGame)
        {
            #region posLock
            if (transform.position.x < minXPoS)
                transform.position = new Vector3(minXPoS, transform.position.y, transform.position.z);
            if (transform.position.x > maxXPos)
                transform.position = new Vector3(maxXPos, transform.position.y, transform.position.z);
            if (transform.position.y < minYPos)
                transform.position = new Vector3(transform.position.x, minYPos, transform.position.z);
            if (transform.position.y > maxYPos)
                transform.position = new Vector3(transform.position.x, maxYPos, transform.position.z);
            #endregion
            #region stamina
            
            if (stamina <= 0)
            {
                //stun
                stu += 100;
                regainStamina = true;
                //Debug.Log(stu + "stu");
            }

            if (regainStamina)
            {
                if (stamina < 100)
                {
                    stamina += (100 / stunDuritation) *Time.deltaTime;
                }
                else
                {
                    stamina = 100;
                    regainStamina = false;
                }
            }
            #endregion
            #region movement
            if (!regainStamina)
            {
                MoveX = Input.GetAxis(Joystick + " x");
                MoveY = Input.GetAxis(Joystick + " y");
                enemies = GameObject.FindGameObjectsWithTag("enemy");
                for (int i = 0; i < GameObject.FindGameObjectsWithTag("enemy").Length; i++)
                {
                    //Debug.Log(enemies[i].GetComponent<badGuyScript>().playerDirectionLocks[playerID -1,0]);
                    if (enemies[i].GetComponent<badGuyScript>().playerDirectionLocks[playerID - 1, 0] == true)
                    {
                        CanMoveNegX = false;
                    }
                    if (enemies[i].GetComponent<badGuyScript>().playerDirectionLocks[playerID - 1, 2] == true)
                    {
                        CanMovePosX = false;
                    }
                    if (enemies[i].GetComponent<badGuyScript>().playerDirectionLocks[playerID - 1, 1] == true)
                    {
                        //  //Debug.Log("fuck dis");
                        CanMovePosY = false;
                    }
                    if (enemies[i].GetComponent<badGuyScript>().playerDirectionLocks[playerID - 1, 3] == true)
                    {
                        CanMoveNegY = false;
                    }
                }
                enemies = null;

                if (CanMovePosX == false && MoveX > 0)
                    MoveX = 0;
                if (CanMoveNegX == false && MoveX < 0)
                    MoveX = 0;
                if (CanMovePosY == false && MoveY > 0)
                    MoveY = 0;
                if (CanMoveNegY == false && MoveY < 0)
                    MoveY = 0;
                CanMoveNegX = true;
                CanMoveNegY = true;
                CanMovePosX = true;
                CanMovePosY = true;
                transform.Translate(new Vector2(MoveX, MoveY) * Time.deltaTime * speed);
                if ((Mathf.Sqrt(MoveX * MoveX + MoveY * MoveY) * Time.deltaTime * speed <= minimumMovement))
                {
                    moving = false;
                }
                else
                {
                    moving = true;
                }


            #endregion
                #region looking

                if (Input.GetAxis(Joystick + " y") > 0 && Input.GetAxis(Joystick + " x") <= Input.GetAxis(Joystick + " y")) //
                {
                    look = 1;
                }
                if (Input.GetAxis(Joystick + " x") > 0 && Input.GetAxis(Joystick + " y") <= Input.GetAxis(Joystick + " x"))
                {
                    look = 2;
                }
                if (Input.GetAxis(Joystick + " y") < 0 && Input.GetAxis(Joystick + " x") >= Input.GetAxis(Joystick + " y"))
                {
                    look = 3;
                }
                if (Input.GetAxis(Joystick + " x") < 0 && Input.GetAxis(Joystick + " y") >= Input.GetAxis(Joystick + " x"))
                {
                    look = 4;
                }
                #endregion
                #region Dash
                if (Input.GetButtonDown(playerID + "A") && !dashing)
                {
                    speed = dashSpeed;
                    dashing = true;
                    //Debug.Log("dash");
                }
                if (dashing)
                {
                    dashTime += Time.deltaTime;
                    if (dashTime > dashLength)
                    {
                
                        dashTime = 0;
                        dashing = false;
                    }
                }
                if (!dashing)
                    speed = baseSpeed;
                #endregion
                #region attack
                attackwait += Time.deltaTime;


                if (Input.GetButtonDown(playerID + "X") && attackwait > attackCooldown)
                {
                    if (anim.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("attack"))
                    {
                        anim.SetTrigger("attack1");
                        //Debug.Log("attack1");
                    }
                    else if (anim.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("attack1"))
                    {
                        anim.SetTrigger("attack");
                        //Debug.Log("attack");
                    }
                    else
                    {
                        ani = UnityEngine.Random.Range(0, 2);
                        //Debug.Log(ani);
                        if (ani == 1)
                        anim.SetTrigger("attack");

                        else if (ani == 0)
                        anim.SetTrigger("attack1");
                    }
                    enemies = GameObject.FindGameObjectsWithTag("enemy");
                    bool one = false;
                    for (int i = 0; i < enemies.Length; i++)
                    {
                        
                        // Debug.Log(enemies[1]);
                        if (Vector2.Angle(enemies[i].transform.position - transform.position, Anime.transform.TransformDirection(Vector3.up)) < attackWidth)
                        {

                            if (Vector2.Distance(transform.position, enemies[i].transform.position) < attackRange)
                            {
                                
                                
                                enemies[i].GetComponent<badGuyScript>().health -= damege;
                                enemies[i].GetComponent<badGuyScript>().lastHit = playerID;
                                if (one == false)
                                {
                                    Instantiate(Hit);
                                    Debug.Log("hit ");
                                }
                                one = true;
                                enemies[i].GetComponent<badGuyScript>().pushed = 2;
                                enemies[i].GetComponent<badGuyScript>().pushedBy = gameObject;
                            }
                        }

                    }
                    #region deleted
                    /*
                
                if (look == 1)
                {
                    for (int i = 0; i < enemies.Length; i++)
                    {

                        if (enemies[i].transform.position.y - transform.position.y > enemies[i].transform.position.x - transform.position.x)
                        {
                            if (Vector2.Distance(enemies[i].transform.position, transform.position) < attackRange)
                            {
                                enemies[i].GetComponent<badGuyScript>().health -= damege;
                                enemies[i].GetComponent<badGuyScript>().lastHit = playerID;
                                Instantiate(Hit);
                            }
                        }
                    }
                }
                if (look == 2)
                {
                    for (int i = 0; i < enemies.Length; i++)
                    {
                        //Debug.Log(i);
                        if (enemies[i].transform.position.x - transform.position.x > enemies[i].transform.position.y - transform.position.y)
                        {
                            if (Vector2.Distance(enemies[i].transform.position, transform.position) < attackRange)
                            {
                                enemies[i].GetComponent<badGuyScript>().health -= damege;
                                enemies[i].GetComponent<badGuyScript>().lastHit = playerID;
                                Instantiate(Hit);
                            }
                        }
                    }
                }
                if (look == 3)
                {
                    for (int i = 0; i < enemies.Length; i++)
                    {
                        //Debug.Log(i);
                        if (transform.position.y - enemies[i].transform.position.y > transform.position.x - enemies[i].transform.position.x)
                        {
                            if (Vector2.Distance(enemies[i].transform.position, transform.position) < attackRange)
                            {
                                enemies[i].GetComponent<badGuyScript>().health -= damege;
                                enemies[i].GetComponent<badGuyScript>().lastHit = playerID;
                                Instantiate(Hit);
                            }
                        }
                    }
                }
                if (look == 4)
                {
                    for (int i = 0; i < enemies.Length; i++)
                    {
                       // //Debug.Log("left" + i);
                        if (transform.position.x - enemies[i].transform.position.x > transform.position.y - enemies[i].transform.position.y)
                        {
                           // //Debug.Log("left" + Vector2.Distance(enemies[i].transform.position, transform.position));
                            if (Vector2.Distance(enemies[i].transform.position, transform.position) < attackRange)
                            {
                                //Debug.Log("stfu");
                                enemies[i].GetComponent<badGuyScript>().health -= damege;
                                enemies[i].GetComponent<badGuyScript>().lastHit = playerID;
                                Instantiate(Hit);
                            }
                        }
                    }
                 
                */
                    #endregion
                    enemies = null;
                    if (Vector2.Angle(otherPlayer.transform.position - transform.position, Anime.transform.TransformDirection(Vector3.up)) < attackWidth && Vector2.Distance(transform.position, otherPlayer.transform.position) < attackRange)
                    {
                        otherPlayer.GetComponent<Movement>().pushed = 2;
                        otherPlayer.GetComponent<Movement>().pushedBy = gameObject;
                    }
                }
                #endregion
                #region pushed
                if (pushed > 0 && pushedBy != null)
                {

                    
                    if (pushed == 1)
                        pushStop = pushedEndTime;
                    else if
                        (pushed == 2)
                        pushStop = smallPushEndTime;
                    //Debug.Log(new Vector2(transform.position.x - (pushedBy.transform.position.x - transform.position.x), transform.position.y - (pushedBy.transform.position.y - transform.position.y)));
                    //Debug.Log(new Vector2((transform.position.x - pushedBy.transform.position.x * -pushedBy.GetComponent<Movement>().pushLength), (transform.position.y - pushedBy.transform.position.y * -pushedBy.GetComponent<Movement>().pushLength)));
                    pushedToPosition = new Vector2(transform.position.x - ((pushedBy.transform.position.x - transform.position.x) * 1000), transform.position.y - ((pushedBy.transform.position.y - transform.position.y)) * 1000);
                    //Debug.Log(pushedToPosition);
                    pushedBy = null;
                    pushedTime = 0;
                }
                if (pushedTime < pushStop)
                {

                    
                    speed = 0;
                    pushedTime += Time.deltaTime;
                    transform.position = Vector2.MoveTowards(transform.position, pushedToPosition, pushSpeed * Time.deltaTime);

                }

                #endregion
                #region Push

                if (Input.GetButtonDown(playerID + "B"))
                {
                    anim.SetTrigger("push");
                    if(!GameObject.FindGameObjectWithTag("push"))
                    Instantiate(push);
                    switch (look)
                    {

                        case 1:
                            enemies = GameObject.FindGameObjectsWithTag("enemy");
                            for (int i = 0; i < enemies.Length; i++)
                            {
                                // Debug.Log(enemies[1]);
                                if (Vector2.Angle(enemies[i].transform.position - transform.position, Anime.transform.TransformDirection(Vector3.up)) < attackWidth)
                                {

                                    if (Vector2.Distance(transform.position, enemies[i].transform.position) < pushRange)
                                    {
                                        enemies[i].GetComponent<badGuyScript>().pushed = 1;
                                        enemies[i].GetComponent<badGuyScript>().pushedBy = gameObject;
                                        //Debug.Log(enemies[i].GetComponent<badGuyScript>().pushedBy);
                                    }
                                }

                            }
                            if (otherPlayer.transform.position.y - transform.position.y > otherPlayer.transform.position.x - transform.position.x)
                            {
                                if (Vector2.Distance(transform.position, otherPlayer.transform.position) < pushRange)
                                {
                                    //otherPlayer.GetComponent<Movement>().canIMove = false;
                                    stu += 50;
                                    otherPlayer.GetComponent<Movement>().pushed = 1;
                                    otherPlayer.GetComponent<Movement>().pushedBy = gameObject;


                                    //   otherPlayer.transform.Translate(new Vector2(otherPlayer.transform.position.x,otherPlayer.transform.position.y+ pushLength));
                                }
                            }

                            break;
                        case 2:
                            enemies = GameObject.FindGameObjectsWithTag("enemy");
                            for (int i = 0; i < enemies.Length; i++)
                            {
                                // Debug.Log(enemies[1]);
                                if (Vector2.Angle(enemies[i].transform.position - transform.position, Anime.transform.TransformDirection(Vector3.up)) < attackWidth)
                                {

                                    if (Vector2.Distance(transform.position, enemies[i].transform.position) < pushRange)
                                    {
                                        enemies[i].GetComponent<badGuyScript>().pushed = 1;
                                        enemies[i].GetComponent<badGuyScript>().pushedBy = gameObject;
                                        //Debug.Log(enemies[i].GetComponent<badGuyScript>().pushedBy);
                                    }
                                }

                            }
                            if (otherPlayer.transform.position.x - transform.position.x > otherPlayer.transform.position.y - transform.position.y)
                            {
                                if (Vector2.Distance(transform.position, otherPlayer.transform.position) < pushRange)
                                {
                                    //otherPlayer.GetComponent<Movement>().canIMove = false;
                                    stu += 50;
                                    otherPlayer.GetComponent<Movement>().pushed = 1;
                                    otherPlayer.GetComponent<Movement>().pushedBy = gameObject;
                                    Debug.Log("pushedPlayer");
                                    //   otherPlayer.transform.Translate(new Vector2(otherPlayer.transform.position.x + pushLength, otherPlayer.transform.position.y));
                                }
                            }
                            break;
                        case 3:
                            enemies = GameObject.FindGameObjectsWithTag("enemy");
                            for (int i = 0; i < enemies.Length; i++)
                            {
                                // Debug.Log(enemies[1]);
                                if (Vector2.Angle(enemies[i].transform.position - transform.position, Anime.transform.TransformDirection(Vector3.up)) < attackWidth)
                                {

                                    if (Vector2.Distance(transform.position, enemies[i].transform.position) < pushRange)
                                    {
                                        enemies[i].GetComponent<badGuyScript>().pushed = 1;
                                        enemies[i].GetComponent<badGuyScript>().pushedBy = gameObject;
                                        //Debug.Log(enemies[i].GetComponent<badGuyScript>().pushedBy);
                                    }
                                }

                            }
                            if (transform.position.y - otherPlayer.transform.position.y > transform.position.x - otherPlayer.transform.position.x)
                            {
                                if (Vector2.Distance(transform.position, otherPlayer.transform.position) < pushRange)
                                {
                                    //otherPlayer.GetComponent<Movement>().canIMove = false;
                                    stu += 50;
                                    otherPlayer.GetComponent<Movement>().pushed = 1;
                                    otherPlayer.GetComponent<Movement>().pushedBy = gameObject;
                                    Debug.Log("pushedPlayer");

                                    //   otherPlayer.transform.Translate(new Vector2(otherPlayer.transform.position.x, otherPlayer.transform.position.y - pushLength));
                                }
                            }
                            break;
                        case 4:
                            enemies = GameObject.FindGameObjectsWithTag("enemy");
                            for (int i = 0; i < enemies.Length; i++)
                            {
                                // Debug.Log(enemies[1]);
                                if (Vector2.Angle(enemies[i].transform.position - transform.position, Anime.transform.TransformDirection(Vector3.up)) < attackWidth)
                                {

                                    if (Vector2.Distance(transform.position, enemies[i].transform.position) < pushRange)
                                    {
                                        enemies[i].GetComponent<badGuyScript>().pushed = 1;
                                        enemies[i].GetComponent<badGuyScript>().pushedBy = gameObject;
                                        //Debug.Log(enemies[i].GetComponent<badGuyScript>().pushedBy);
                                    }
                                }
                                if (transform.position.y - enemies[i].transform.position.y - transform.position.y < transform.position.x - enemies[i].transform.position.x)
                                { }

                            }
                            if (transform.position.x - otherPlayer.transform.position.x > transform.position.y - otherPlayer.transform.position.y)
                            {
                                if (Vector2.Distance(transform.position, otherPlayer.transform.position) < pushRange)
                                {
                                    //otherPlayer.GetComponent<Movement>().canIMove = false; 
                                    stu += 50;
                                    otherPlayer.GetComponent<Movement>().pushed = 1;
                                    otherPlayer.GetComponent<Movement>().pushedBy = gameObject;
                                    Debug.Log("pushedPlayer");
                                    //Debug.Log("left pushed");

                                    //otherPlayer.transform.Translate(new Vector2(otherPlayer.transform.position.x-pushLength,otherPlayer.transform.position.y));
                                }
                            }
                            break;
                    }

                }

                #endregion
            }
        }
    }

    void OnTriggerEnter(Collider collision)
    {
        Debug.Log(collision);
        if (dashing)
        {
            if (collision.tag == "player1" || collision.tag == "player2")
            {
                collision.GetComponent<Movement>().pushed = 2;
                collision.GetComponent<Movement>().pushedBy = gameObject;
            }
            if (collision.tag == "enemy")
            {
                collision.GetComponent<badGuyScript>().pushed = 2;
                collision.GetComponent<badGuyScript>().pushedBy = gameObject;
            }
            
        }
        //Debug.Log(collision);
        #region coin
        /*if (collision.tag=="coin")
        {
            coin++;
            Destroy(collision.gameObject);
        }*/
        #endregion
        #region movecol
        if (collision.tag != "coin")
        {
            // //Debug.Log(transform.position.x - collision.transform.position.x);
            if (transform.position.x > collision.transform.position.x)
            {
                if (collision.tag == "enemy")
                {
                    collision.GetComponent<badGuyScript>().playerDirectionLocks[playerID-1, 0] = true ;
                    //Debug.Log(collision.GetComponent<badGuyScript>().playerDirectionLocks[playerID - 1, 0]+"0");
                }
                
            }
            else if (transform.position.x < collision.transform.position.x)
            {
                if (collision.tag == "enemy")
                {
                    
                    collision.GetComponent<badGuyScript>().playerDirectionLocks[playerID-1 , 2] = true;
                    //Debug.Log(collision.GetComponent<badGuyScript>().playerDirectionLocks[playerID - 1, 2] + "2");
                }
                //CanMoveNegX = true;
               // CanMovePosX = false;
                // //Debug.Log(CanMoveNegX+"-x");
            }
            

            if (transform.position.y < collision.transform.position.y)
            {
                if (collision.tag == "enemy")
                {
                    collision.GetComponent<badGuyScript>().playerDirectionLocks[playerID-1, 1] = true;
                    //Debug.Log(collision.GetComponent<badGuyScript>().playerDirectionLocks[playerID - 1, 1]+"1");
                }
                
                //CanMoveNegY = true;
               // CanMovePosY = false;
                // //Debug.Log(CanMoveNegY+"-y");
            }
            else if (transform.position.y > collision.transform.position.y)
            {
                if (collision.tag == "enemy")
                {
                    collision.GetComponent<badGuyScript>().playerDirectionLocks[playerID-1, 3] = true;
                    //Debug.Log(collision.GetComponent<badGuyScript>().playerDirectionLocks[playerID - 1, 3]+"3");
                }
               // CanMoveNegY = false;
               // CanMovePosY = true;
                // //Debug.Log(CanMovePosY+"+y");
            }
            
        }
        #endregion 
    }
    void OnTriggerExit(Collider collisionExit)
    {
        if (collisionExit.tag == "enemy")
        {
            for (int k = 0; k < 4; k++)
            {
                collisionExit.GetComponent<badGuyScript>().playerDirectionLocks[playerID - 1, k] = false;
                //Debug.Log(k);
            }
        }
        
    }
}