﻿using UnityEngine;
using System.Collections;

public class Movmentplayer2 : MonoBehaviour {

    public float MoveX;
    public float MoveY;

    public bool FreeMove = true;
    public bool CanMovePosX = true;
    public bool CanMoveNegX = true;
    public bool CanMovePosY = true;
    public bool CanMoveNegY = true;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        //trying to makeing movement smooth
        //targetPosition = new Vector2(Input.GetAxis("joystick 1 x"), Input.GetAxis("joystick 1 y"));
        //transform.position = Vector2.Lerp(transform.position, targetPosition, Time.deltaTime * smoothFactor);

        MoveX = Input.GetAxis("Joystick 2 x");

        MoveY = Input.GetAxis("Joystick 2 y");

        // Debug.Log("x=" + Input.GetAxis("joystick 1 x") + "y= " + Input.GetAxis("joystick 1 y"));



        if (CanMovePosX == false && MoveX > 0)
            MoveX = 0;
        if (CanMoveNegX == false && MoveX < 0)
            MoveX = 0;
        if (CanMovePosY == false && MoveY > 0)
            MoveY = 0;
        if (CanMoveNegY == false && MoveY < 0)
            MoveY = 0;

        transform.Translate(new Vector2(MoveX, MoveY) * Time.deltaTime * 3);



    }
    void OnTriggerEnter(Collider collision)
    {
        Debug.Log(transform.position.x - collision.transform.position.x);
        if (transform.position.x > collision.transform.position.x)
        {
            FreeMove = false;
            CanMoveNegX = false;
            CanMovePosX = true;
            Debug.Log(CanMovePosX + "+x");
        }
        else if (transform.position.x < collision.transform.position.x)
        {
            FreeMove = false;
            CanMoveNegX = true;
            CanMovePosX = false;
            Debug.Log(CanMoveNegX + "-x");
        }
        else
        {
            FreeMove = true;
            CanMoveNegX = true;
            CanMovePosX = true;
        }

        if (transform.position.y < collision.transform.position.y)
        {
            FreeMove = false;
            CanMoveNegY = true;
            CanMovePosY = false;
            Debug.Log(CanMoveNegY + "-y");
        }
        else if (transform.position.y > collision.transform.position.y)
        {
            FreeMove = false;
            CanMoveNegY = false;
            CanMovePosY = true;
            Debug.Log(CanMovePosY + "+y");
        }
        else
        {
            FreeMove = true;
            CanMoveNegY = true;
            CanMovePosY = true;
        }


        /*if (transform.position.y >= collision.transform.position.y)
        {
            FreeMove = false;
            CanMoveNegX = true;
            CanMovePosX = true;
            CanMoveNegY = false;
            CanMovePosY = true;
        }
        if (transform.position.y >= collision.transform.position.y)
        {
            FreeMove = false;
            CanMoveNegX = true;
            CanMovePosX = true;
            CanMoveNegY = true;
            CanMovePosY = false;
        }*/
        Debug.Log(collision);
        //canmove = false;
    }
    void OnTriggerExit(Collider collision)
    {
        FreeMove = true;
        CanMoveNegX = true;
        CanMovePosX = true;
        CanMovePosY = true;
        CanMoveNegY = true;
    }
}
