﻿using UnityEngine;
using System.Collections;

public class Player2anime : MonoBehaviour {

    public float xAxis;
    public float yAxis;
    public float Rotation;
    public int look;
    private Movement moving;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
        moving = GameObject.Find("Player 2").GetComponent<Movement>();
        if (moving.moving == true)
            GetComponent<Animator>().SetBool("Walking", true);
        if (moving.moving == false)
            GetComponent<Animator>().SetBool("Walking", false);
        
        Quaternion targetRot = Quaternion.Euler(0, 0, Rotation+180);
        transform.rotation = Quaternion.Slerp(this.transform.rotation, targetRot, 10 * Time.deltaTime);
        //transform.rotation = new Quaternion(0, 0, Rotation, 1);
        xAxis = Input.GetAxis("joystick 2 x");
        yAxis = Input.GetAxis("Rotation 2 Y");
        if (yAxis == 0 && xAxis == 0)
        { }
        else if (yAxis >= 0 && xAxis >= 0)
        {
            Rotation = (xAxis - yAxis) * 45 + 45;
        }

        else if (yAxis < 0 && xAxis >= 0)
        {
            //yAxis = (yAxis * -1);
            Rotation = (-xAxis - yAxis) * 45 + 135;

        }
        else if (yAxis < 0 && xAxis < 0)
        {
            Rotation = (-xAxis + yAxis) * 45 + 225;
        }
        else if (yAxis >= 0 && xAxis < 0)
        {
            Rotation = (xAxis + yAxis) * 45 + 315;
        }
        
	}
}
