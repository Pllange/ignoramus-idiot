﻿using UnityEngine;
using System.Collections;

public class SpawnManeger : MonoBehaviour
{
    public Vector3[] spawnPoint ;
    public GameObject enemy;
    public int whichSpawn;
    public bool debugHelper;
    public float spawn;
    public float spawnCooldown;
    public float spawnWait;
    public float moreSpawns;
    public bool game;
    private bool endgame;
    public GameManeger gamemaneger;
    public int moreSpawnDoubler;
    private GameObject[] lol;
    public GameObject gate0;
    public GameObject gate1;
    public GameObject gate2;
    public GameObject gate3;
    private int oldSpawn;
    // Use this for initialization
    void Start()
    {
        
        gamemaneger = GameObject.FindGameObjectWithTag("GameManeger").GetComponent<GameManeger>();
        spawnPoint = gamemaneger.spawnPoint;
        spawnCooldown = gamemaneger.spawnCooldown;
        moreSpawns = gamemaneger.moreSpawns;
        moreSpawns = 1 / moreSpawns;
    }

    // Update is called once per frame
    void Update()
    {
        moreSpawnDoubler = GameObject.FindGameObjectsWithTag("enemy").Length;
    
        
        

        
        endgame = Camera.main.GetComponent<GUIMan>().endGame;
        game = Camera.main.GetComponent<GUIMan>().inGame;
        if(game && !endgame)
        spawnWait += Time.deltaTime;
        if (spawnWait >  spawnCooldown)
        {
            spawn += 1;
            spawnWait = 0;
        }
        if (spawn > 1)
        {
            
            Spawn();
        }
        
    }
    void Spawn()
    {
        spawn -= 1;
        while (oldSpawn == whichSpawn)
        {
            whichSpawn = Random.Range(0, spawnPoint.Length);
        }

        oldSpawn = whichSpawn;
        Instantiate(enemy,spawnPoint[whichSpawn],new Quaternion(0,0,0,0));
        switch (whichSpawn)
        {
            case 0:
                gate0.GetComponent<Animator>().SetTrigger("Open");
                if (debugHelper)
                    Debug.Log("1");
                break;
            case 1:
                if(gate1.GetComponent<Animator>())
                gate1.GetComponent<Animator>().SetTrigger("Open");
                if (debugHelper)
                    Debug.Log("2");
                break;
            case 2:
                gate2.GetComponent<Animator>().SetTrigger("Open");
                if (debugHelper)
                    Debug.Log("3");
                break;
            case 3:
                gate3.GetComponent<Animator>().SetTrigger("Open");
                if (debugHelper)
                    Debug.Log("4");
                break;
            default:
                Debug.Log("something fucked up with spawning, call your nearest peter");
                break;
        }

    }

}