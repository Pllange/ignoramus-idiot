﻿using UnityEngine;
using System.Collections;

public class badGuyScript : MonoBehaviour {
    private float attackingWait;
    private float findTargetWait;
    private float distanceToPlayer1;
    private float distanceToPlayer2;
    private float pushedTime;
    
    public float pushLength;
    public float pushedEndTime;
    public float beingPushed;
    public float findTargetCooldown;
    public float attackRange; // the range before he starts strikking
    public float attackMaxRange; //the range on the actual attack
    public float attackingEndTime; //the time it takes to attack
    public float attackValue; //damege done
    public int health;
    public float speed;
    public int pushed;
    private bool findTarget;
    private bool playerTargeted;
    private bool attacking;
    public bool[,] playerDirectionLocks = new bool[2,4];
    public GameObject[] players;
    public GameObject target;
    private GameObject nearestPlayer;
    public GameObject remains;
    public GameObject Dam;
    public GameObject death;
    public Vector2 targetPosition;
    public Vector2 pushedToPosition;
    public GameObject pushedBy;
    private SpawnManeger spawnManeger;
    public int lastHit;
    public GameManeger gamemaneger;
    public int coinsDropped;
    public float distanceFromPlayer;
    public float pushSpeed;
    public float baseSpeed;
    public float smallPushEndTime;
    public float minXPoS;
    public float minYPos;
    public float maxXPos;
    public float maxYPos;
    public float minMove;
    public bool stillStanding;
   


	// Use this for initialization
	void Start () {
        gamemaneger = GameObject.FindGameObjectWithTag("GameManeger").GetComponent<GameManeger>();
        findTargetCooldown = gamemaneger.findTargetCooldown;
        attackRange= gamemaneger.badGuysAttackRange;
        attackMaxRange= gamemaneger.attackMaxRange;
        attackingEndTime = gamemaneger.attackingEndTime;
        attackValue = gamemaneger.attackValue;
        health = gamemaneger.health;
        speed = gamemaneger.speed;
        distanceFromPlayer = gamemaneger.distanceFromPlayer;
        coinsDropped = gamemaneger.coinsDropped;
        pushedEndTime = gamemaneger.pushedEndTime;
        pushLength = gamemaneger.pushLength;
        pushSpeed = gamemaneger.beingPushedSpeed;
        baseSpeed = speed;
        smallPushEndTime = gamemaneger.badSmallPushedEndTime;
        minXPoS = gamemaneger.minXPoS;
        minYPos = gamemaneger.minYPos;
        maxXPos = gamemaneger.maxXPos;
        maxYPos = gamemaneger.maxYPos;


        if (gamemaneger.bigMonsters)
        {
            health = gamemaneger.bigMonsterHealth;
            speed = gamemaneger.bigMonsterSpeed;
            gameObject.transform.localScale = new Vector2(gamemaneger.bigMonsterScale, gamemaneger.bigMonsterScale);
            distanceFromPlayer = gamemaneger.bigMonstersDistanceFromPlayer;
            coinsDropped = gamemaneger.bigMonsterscoinsDropped;
        }

        target = GameObject.FindGameObjectWithTag("SpawnManeger");
        spawnManeger = GameObject.FindGameObjectWithTag("SpawnManeger").GetComponent<SpawnManeger>();
        players[0] = GameObject.FindGameObjectWithTag("player1");

        players[1] = GameObject.FindGameObjectWithTag("player2");
       // target = GameObject.FindGameObjectWithTag("player1");
        targetPosition = new Vector2(0, 0);
        for (int i = 0; i < 2; i++)
        {
            for (int k = 0; k < 4; k++)
            {
                playerDirectionLocks[i, k] = false;
                //Debug.Log(i + " " + k);
            }
        }
	}

    // Update is called once per frame
    void Update()
    {
        #region posLock
        if (transform.position.x < minXPoS)
            transform.position = new Vector3(minXPoS, transform.position.y, transform.position.z);
        if (transform.position.x > maxXPos)
            transform.position = new Vector3(maxXPos, transform.position.y, transform.position.z);
        if (transform.position.y < minYPos)
            transform.position = new Vector3(transform.position.x, minYPos, transform.position.z);
        if (transform.position.y > maxYPos)
            transform.position = new Vector3(transform.position.x, maxYPos, transform.position.z);
        #endregion
        #region FindingTarget
        findTargetWait += Time.deltaTime;
        if (findTargetWait > findTargetCooldown)
        {
            findTarget = true;
            findTargetWait = 0;
            playerTargeted = true;
        }
        if (findTarget)
        {
            findTarget = false;
            distanceToPlayer1 = Vector2.Distance(transform.position, players[0].transform.position);
            distanceToPlayer2 = Vector2.Distance(transform.position, players[1].transform.position);
            if (distanceToPlayer1 > distanceToPlayer2)
            {
                target = players[1];
                //playerTargeted = true;
            }
            if (distanceToPlayer2 > distanceToPlayer1)
            {
                target = players[0];
                // 
            }
        }
        #endregion
        #region Movement
        speed = baseSpeed;
        if (playerTargeted)
            targetPosition = target.transform.position;
        if (attacking == true || Vector2.Distance(transform.position, targetPosition) < distanceFromPlayer)
            targetPosition = transform.position;
        if (pushed > 0)
        {
            targetPosition = pushedToPosition;
            beingPushed += Time.deltaTime;
            
            speed = pushSpeed;
            if (beingPushed > pushedEndTime)
            {
                pushed = 0;
                beingPushed = 0;
            }
            if (pushed > 1 && beingPushed > smallPushEndTime)
            {
                pushed = 0;
                beingPushed = 0;
            }
        }
        if (Vector2.Distance(Vector2.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime), transform.position) < minMove)
            stillStanding = true;
        else
            stillStanding = false;

            transform.position = Vector2.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);
            //Debug.Log(Vector3.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime));
        
        #endregion
        #region attacking
        if (gamemaneger.playerHealth)
        {
            
            if (attacking)
                attackingWait += Time.deltaTime;

            if (attackingWait >= attackingEndTime)
            {
                attackingWait = 0;
                attacking = false;
                if (Vector2.Distance(transform.position, target.transform.position) < attackMaxRange)
                    if (!target.GetComponent<Movement>().regainStamina)
                    {
                        Instantiate(Dam);
                        target.GetComponent<Movement>().stamina -= attackValue;
                        stillStanding = false;
                    }

            }
            if (Vector2.Distance(transform.position, target.transform.position) < attackRange)
            {
                
                attacking = true;
                if (!gameObject.GetComponentInChildren<Animator>().GetCurrentAnimatorStateInfo(0).IsName("attack"))
                gameObject.GetComponentInChildren<rotation>().attack();
            }
        }

        #endregion
        #region Death

        if (health < 1)
        {
            players[lastHit - 1].GetComponent<Movement>().killz += 1;
            //Instantiate(coin, transform.position, new Quaternion(0, 0, 0, 0));
            //Instantiate(remains, transform.position, new Quaternion(0, 0, 0, 0));
            if (spawnManeger.game)
                spawnManeger.spawn += 1 + (spawnManeger.moreSpawns / spawnManeger.moreSpawnDoubler);
            Destroy(gameObject);
            Instantiate(death, transform.position, new Quaternion(0, 0, 0, 0));

        }

        #endregion
        #region push
        if (pushed > 0 && pushedBy !=null)
        {
            pushedToPosition = new Vector2(transform.position.x - ((pushedBy.transform.position.x - transform.position.x) * 1000), transform.position.y - ((pushedBy.transform.position.y - transform.position.y)) * 1000);
            pushedBy = null;
            
        }
        
    }

#endregion
}
