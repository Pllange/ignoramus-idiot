﻿using UnityEngine;
using System.Collections;

public class rotation : MonoBehaviour 
{
    public Vector3 target;
	// Use this for initialization
	void Start () {
        GetComponent<Animator>().SetBool("walk", true);
	}
	
	// Update is called once per frame
	void Update () {
        if (!gameObject.GetComponentInParent<badGuyScript>().stillStanding)
            GetComponent<Animator>().SetBool("walk", true);
        else
            GetComponent<Animator>().SetBool("walk", false);
        if (Input.GetKey(KeyCode.A) && Input.GetKeyDown(KeyCode.D))
        {
            Debug.Log("you are badass");
            GetComponent<Animator>().SetTrigger("BadAss");
        }
        target=GetComponentInParent<badGuyScript>().target.transform.position;
        //transform.LookAt(new Vector3(target.x,target.y,-1));
        Vector3 diff = target - transform.position;
        diff.Normalize();

        float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);
	}
    
    public void attack()
    {
        GetComponent<Animator>().SetTrigger("attack");
    }
}
