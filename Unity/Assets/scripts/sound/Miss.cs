﻿using UnityEngine;
using System.Collections;

public class Miss : MonoBehaviour
{
    private AudioSource pickUp;
    public AudioClip miss1;
    public AudioClip miss2;
    private int miss;
    // Use this for initialization
    void Start()
    {
        pickUp = gameObject.GetComponent<AudioSource>();
        pickUp.Play();
        miss = Random.Range(0, 2);
    }

    // Update is called once per frame
    void Update()
    {
        if (miss == 1)
        {
            pickUp.clip = miss1;
        }
        if (miss == 2)
        {
            pickUp.clip = miss2;
        }

        if (!pickUp.isPlaying)
            Destroy(gameObject);


    }

}
