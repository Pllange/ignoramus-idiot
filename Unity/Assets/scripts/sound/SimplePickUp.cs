﻿using UnityEngine;
using System.Collections;

public class SimplePickUp : MonoBehaviour {
    private AudioSource pickUp;
	// Use this for initialization
	void Start () {
	pickUp= gameObject.GetComponent<AudioSource>();
    pickUp.Play();
	}
	
	// Update is called once per frame
	void Update () 
    {
        
        if (!pickUp.isPlaying)
            Destroy(gameObject);
        
        
	}

}
