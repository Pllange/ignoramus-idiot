﻿using UnityEngine;
using System.Collections;

public class backgroundmusic : MonoBehaviour {
    private AudioSource background;
    private bool playmusic;
	// Use this for initialization
	void Start () 
    {
        background = gameObject.GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        if(Input.GetKeyDown(KeyCode.M) && background.isPlaying)
        {
            background.Stop();
        }
        else if (Input.GetKeyDown(KeyCode.M) && !background.isPlaying)
        {
            background.Play();
            background.volume = 0;
        }
        if(background.volume <= 0.2f)
        background.volume += 0.1f*Time.deltaTime;
	}
}
