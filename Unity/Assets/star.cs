﻿using UnityEngine;
using System.Collections;

public class star : MonoBehaviour {
    public bool start;
    public bool running;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (gameObject.GetComponentInParent<Movement>().regainStamina && !running)
            start = true;
        else
            start = false;
        if (gameObject.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("stun"))
            running = true;
        else
            running = false;
        if (start == true && running==false)
        {
            gameObject.GetComponent<Animator>().SetTrigger("star");
            
        }
        
        
	}
}
