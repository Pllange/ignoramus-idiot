﻿using UnityEngine;
using System.Collections;

public class testmovement : MonoBehaviour {
    
    public float MoveX;
    public float MoveY;
    public string Joystick;

    public bool FreeMove = true;
    public bool CanMovePosX = true;
    public bool CanMoveNegX = true;
    public bool CanMovePosY = true;
    public bool CanMoveNegY = true;
    public float stamina;
    public int coin;
    public int look;
    public float stu;

	// Use this for initialization
	void Start () 
    {
        look = 1;
	}
	
	// Update is called once per frame
	void Update ()
    {
        #region stamina
        if (stamina <= 0)
        {
            //stun
            stu += 100;
            stamina += 100;
        }
        if (stu > 0)
        {
            stu--;
        }
        #endregion
        Debug.Log("x" +Input.GetAxis(Joystick + " x") + " y" + Input.GetAxis(Joystick + " y"));
        Debug.Log(Input.GetButtonDown("2A"));

        MoveX = Input.GetAxis(Joystick+" x");
        
        MoveY = Input.GetAxis(Joystick+" y");
        if (CanMovePosX == false && MoveX > 0)
            MoveX = 0;
        if (CanMoveNegX == false && MoveX < 0)
            MoveX = 0;
        if (CanMovePosY == false && MoveY > 0)
            MoveY = 0;
        if (CanMoveNegY == false && MoveY < 0)
            MoveY = 0;
        if(stu<=0)
        transform.Translate(new Vector2(MoveX, MoveY) * Time.deltaTime * 3);
        #region looking
        if (MoveY == 0 && MoveX == 0)
        {
            look = 0;
        }
        if (MoveY > 0 && MoveX <= MoveY)
        {
            look = 1;
        }
        if (MoveX > 0 && MoveY <= MoveX)
        {
            look = 2;
        }
        if (MoveY < 0 && MoveX >= MoveY)
        {
            look = 3;
        }
        if (MoveX < 0 && MoveY >= MoveX)
        {
            look = 4;
        }
        #endregion
    }
    void OnTriggerEnter(Collider collision)
    {
        Debug.Log(collision);
        #region coin
        if (collision.tag=="coin")
        {
            coin++;
            Destroy(collision.gameObject);
        }
        #endregion
        #region movecol
       // Debug.Log(transform.position.x - collision.transform.position.x);
        if (transform.position.x > collision.transform.position.x)
        {
            FreeMove = false;
            CanMoveNegX = false;
            CanMovePosX = true;
            //Debug.Log(CanMovePosX+"+x");
        }
        else if (transform.position.x < collision.transform.position.x)
        {
            FreeMove = false;
            CanMoveNegX = true;
            CanMovePosX = false;
           // Debug.Log(CanMoveNegX+"-x");
        }
        else
        {
            FreeMove = true;
            CanMoveNegX = true;
            CanMovePosX = true;
        }

        if (transform.position.y < collision.transform.position.y)
        {
            FreeMove = false;
            CanMoveNegY = true;
            CanMovePosY = false;
           // Debug.Log(CanMoveNegY+"-y");
        }
        else if (transform.position.y > collision.transform.position.y)
        {
            FreeMove = false;
            CanMoveNegY = false;
            CanMovePosY = true;
           // Debug.Log(CanMovePosY+"+y");
        }
        else
        {
            FreeMove = true;
            CanMoveNegY = true;
            CanMovePosY = true;
        }
        #endregion


        /*if (transform.position.y >= collision.transform.position.y)
        {
            FreeMove = false;
            CanMoveNegX = true;
            CanMovePosX = true;
            CanMoveNegY = false;
            CanMovePosY = true;
        }
        if (transform.position.y >= collision.transform.position.y)
        {
            FreeMove = false;
            CanMoveNegX = true;
            CanMovePosX = true;
            CanMoveNegY = true;
            CanMovePosY = false;
        }*/
        //Debug.Log(collision);
            //canmove = false;
    }
    void OnTriggerExit(Collider collision)
    {
        FreeMove = true;
        CanMoveNegX = true;
        CanMovePosX = true;
        CanMovePosY = true;
        CanMoveNegY = true;
    }
}
